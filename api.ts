import { API_URL } from '@/constants/config'
import { IHypothesisRawData } from '@/models/charts'
import { IAuthResponse } from '@/models/main'

import Api, { IGet, IPost } from './Api'

export const api = new Api({ host: API_URL })

/* eslint-disable max-len */
export const authApi = api.endpoint<IPost<IAuthResponse>>('user/authenticate')
export const hypothesisOneApi = api.endpoint<IGet<IHypothesisRawData>>('screen_6/')
