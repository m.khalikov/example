import { computed, defineComponent, onMounted } from 'vue'
import { ChartOptions } from 'chart.js'
import { useStore } from '@/store'
import { EActionTypes } from '@/store/actionTypes'
import { useMobile } from '@/hooks/useMobile'
import { DEFAULT_LINE_CHART_OPTIONS } from '@/constants/charts'
import LoaderStub from '@/components/LoaderStub'
import { LineChart } from '@/components/Chart/Chart'
import './Hypothesis.sass'

const OPTIONS: ChartOptions = {
  ...DEFAULT_LINE_CHART_OPTIONS,
  annotation: {
    annotations: [
      {
        drawTime: 'afterDatasetsDraw',
        id: 'vline',
        type: 'line',
        mode: 'vertical',
        scaleID: 'x-axis-0',
        value: 11,
        borderColor: '#999999',
        borderWidth: 1,
        borderDash: [4, 4],
      },
      {
        drawTime: 'afterDatasetsDraw',
        id: 'vline2',
        type: 'line',
        mode: 'vertical',
        scaleID: 'x-axis-0',
        value: 14.5,
        borderColor: '#999999',
        borderWidth: 1,
        borderDash: [4, 4],
      },
    ],
  },
}

export default defineComponent({
  name: 'Hypothesis',
  setup () {
    const { dispatch, state } = useStore()
    const { isMobile } = useMobile()

    onMounted(async () => dispatch(EActionTypes.GET_HYPOTHESIS_DATA))

    const data = computed<IHypothesisData>(() => state.hypothesisData)

    return () => {
      return (
        <div class='Hypothesis data-section'>
          {!isMobile.value && <h1>Hypothesis: ***</h1>}

          <div class='Hypothesis__wrapper'>
            {data.value ? (
              <LineChart
                data={data}
                options={OPTIONS}
                height={600}
              />
              ) : (
                <LoaderStub />
              )}
          </div>
        </div>
      )
    }
  },
})
