import { IHypothesisChartData } from '@/models/charts'
import { Nullable } from '@/models/common'

export interface IRootState {
  isMenuOpen: boolean
  hypothesisOneData: Nullable<IHypothesisChartData>
}

export const state: IRootState = {
  isMenuOpen: false,
  hypothesisOneData: null,
}
