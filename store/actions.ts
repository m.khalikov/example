import { ActionContext, ActionTree } from 'vuex'
import { hypothesisOneApi } from '@/api'
import { hypothesisRawDataToChart } from '@/utils/charts'
import { IRootState } from './state'
import { EMutationTypes } from './mutationTypes'
import { EActionTypes } from './actionTypes'
import { Mutations } from './mutations'
import { IHypothesisChartData } from '@/models/charts'

export type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload?: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>
} & Omit<ActionContext<IRootState, IRootState>, 'commit'>

export interface IActions {
  [EActionTypes.TOGGLE_MENU](
    { commit }: AugmentedActionContext
  ): void

  [EActionTypes.GET_HYPOTHESIS_DATA](
    { commit }: AugmentedActionContext
  ): Promise<IHypothesisChartData[]>
}

export const actions: ActionTree<IRootState, IRootState> & IActions = {
  [EActionTypes.TOGGLE_MENU]: ({ commit }) => {
    commit(EMutationTypes.TOGGLE_MENU)
  },

  [EActionTypes.GET_HYPOTHESIS_DATA]: async ({ commit }) => {
    const rawData = await hypothesisOneApi.get()
    const data = hypothesisRawDataToChart(rawData)

    commit(EMutationTypes.SET_HYPOTHESIS_ONE_DATA, data)

    return data
  },
}
