import { MutationTree } from 'vuex'
import { IHypothesisChartData } from '@/models/charts'
import { EMutationTypes } from './mutationTypes'
import { IRootState } from './state'

export type Mutations<S = IRootState> = {
  [EMutationTypes.TOGGLE_MENU]: (state: S) => void
  [EMutationTypes.SET_HYPOTHESIS_ONE_DATA]: (state: S, payload: IHypothesisChartData) => void
}

export const mutations: MutationTree<IRootState> & Mutations = {
  [EMutationTypes.TOGGLE_MENU]: (state) => {
    state.isMenuOpen = !state.isMenuOpen
  },

  [EMutationTypes.SET_HYPOTHESIS_ONE_DATA]: (state, payload) => {
    state.hypothesisOneData = payload
  },
}
