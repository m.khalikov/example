import {
  computed,
  defineComponent,
  Transition,
} from 'vue'
import { useStore } from '@/store'
import { useMobile } from '@/hooks/useMobile'
import { sidebarNavigation } from '@/constants/sidebarNavigation'
import { EActionTypes } from '@/store/actionTypes'
import Burger from '@/components/Burger/Burger'
import Navigation from '@/components/Navigation/Navigation'
import './Sidebar.sass'

export default defineComponent({
  name: 'Sidebar',
  setup () {
    const { state, dispatch } = useStore()
    const { isMobile } = useMobile()

    const isMenuOpen = computed(() => state.isMenuOpen)

    const handleBackdropClick = () => dispatch(EActionTypes.TOGGLE_MENU)

    return () => (
      <div class='Sidebar'>
        {isMobile.value && <Burger />}
        <div class='Sidebar__logo'>
          <img src='/img/svg/logo.svg' />
          <img src='/img/svg/by-ark.svg' />
        </div>

        <hr />

        <Navigation navigation={sidebarNavigation} isMenuOpen={isMenuOpen.value} />

        <Transition name='fade'>
          <div class='Sidebar__backdrop' v-show={isMobile.value && isMenuOpen.value} onClick={handleBackdropClick} />
        </Transition>
      </div>
    )
  },
})
