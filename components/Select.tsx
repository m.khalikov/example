import { defineComponent, PropType, ref } from 'vue'
import cn from 'classnames'
import { Nullable } from '@/models/common'
import './Select.sass'

type ChangeEvent = (val: IOption) => void

export interface IOption<T = any> {
  value: T
  label: string
  data?: T
}

export default defineComponent({
  name: 'Select',
  props: {
    options: {
      required: true,
      type: Object as PropType<IOption[]>,
    },
    value: {
      type: Object as PropType<IOption>,
      default: null,
    },
    placeholder: {
      type: String as PropType<string>,
      default: '',
    },
    onChange: {
      type: Function as PropType<ChangeEvent>,
      required: true,
    },
  },
  setup (props) {
    const current = ref<Nullable<IOption>>(null)
    const isExpanded = ref<boolean>(false)

    const handleSelect = (val: IOption) => () => {
      props.onChange(val)
      current.value = val
      isExpanded.value = false
    }

    const handleWrapperClicked = () => {
      isExpanded.value = !isExpanded.value
    }

    return () => (
      <div class='Select'>
        <div class='Select__wrapper' onClick={handleWrapperClicked}>
          {current.value?.label || props.value?.label || props.placeholder}
          <img class={cn({ 'rotated': isExpanded.value })} src="/img/svg/triangle.svg"/>
        </div>

        <div class='Select__options-wrapper' v-show={isExpanded.value}>
          {props.options.map(o => (
            <span
              key={o.label}
              onClick={handleSelect(o)}
              class={cn({ 'is-selected': current.value === o.value })}
            >
              {o.label}
            </span>
          ))}
        </div>
      </div>
    )
  },
})
